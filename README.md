# README.md

## 初始化流程
- 建立外部資料夾 KarmaCam
- karma-cam-releace 重新命名為 karma-cam
- 將 karma-cam 搬移至 KarmaCam 中
- 將 Launcher 從 karma-cam 搬移至 KarmaCam 中
- 最終完整路徑參考
    ```
    - KarmaCam /
        ∟ karma-cam /
            ∟ Video_Capture.exe
            ∟ karma_cam_config.ini
            ∟ version.ini
            ∟ public_key.pem
            ∟ License
            ∟ dll /
            ∟ img / 
        ∟ Launcher.exe
        ∟ Video /
            ∟ date /
    ```